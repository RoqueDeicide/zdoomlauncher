A simple launcher for zDoom that uses WPF for interface.

It has to be placed into the same folder as zDoom, IWADs, WADs, and it uses command line arguments to do everything with source-port.

Make sure .NET 4.5, 4.0 and probably 3.5 are installed.

Status: RC1.